setup:
	pre-commit autoupdate
	pre-commit install

format:
	isort .
	black -v .

lint:
	flake8 .

precommit:
	pre-commit run --all-files
