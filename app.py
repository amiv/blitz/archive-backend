# -*- coding: utf-8 -*-
from os import getenv

from flask import Flask
from flask_cors import CORS
from yaml import safe_load

from api import api

with open(getenv('CONFIG', 'config.yaml')) as cfg:
    config = safe_load(cfg)

app = Flask(__name__)

app.config.update(
    API_URL=config.get('api_url', 'localhost'),
    DATA_DIR=config.get('data_dir', 'localhost'),
    BASE_DIR=getenv('ROOT_DIR', '/data'),
    ALLOWED_FILENAMES=['pdf'],
    REQUIRED_GROUP=config.get('required_group', None),
)

CORS(app)
api.init_app(app)
