# -*- coding: utf-8 -*-
import os

# for 1960 to 2022 create a folder containing the directories HS and FS


def create():
    """Creates the directories for the data."""
    for year in range(1960, 2023):
        for dir_ in ['HS', 'FS']:
            path = os.path.join('data', str(year), dir_)
            if not os.path.exists(path):
                os.makedirs(path)


def move():
    """Moves the data from the old structure to the new structure."""
    for year in range(1960, 2023):
        for dir_ in ['HS', 'FS']:
            old_path = os.path.join('data', str(year) + dir_)
            new_path = os.path.join('data', str(year), dir_)
            print(old_path, new_path)
            if os.path.exists(old_path):
                for file_ in os.listdir(old_path):
                    os.rename(os.path.join(old_path, file_), os.path.join(new_path, file_))
                os.rmdir(old_path)


if __name__ == '__main__':
    create()
    move()
