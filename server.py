# -*- coding: utf-8 -*-
# wsgi server (used in docker container)
# [bjoern](https://github.com/jonashaag/bjoern) required.

import bjoern

from app import app

if __name__ == '__main__':
    print('Starting bjoern on port 8080...', flush=True)
    bjoern.run(app, '0.0.0.0', 8080)  # noqa
