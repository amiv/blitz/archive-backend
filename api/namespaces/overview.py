# -*- coding: utf-8 -*-
from datetime import datetime

from flask import current_app
from flask_restx import Namespace, Resource

from api.utils.files import fs_tree_to_dict

api = Namespace('overview', description='Overview over the available blitzes')


@api.route('')
class Overview(Resource):
    def get(self):
        current_year = int(datetime.now().year)

        return {
            'overview': fs_tree_to_dict(current_app.config['DATA_DIR']),
            'current_century': int((current_year // 10) * 10),
            'current_year': current_year,
            'current_semester': 'FS' if datetime.now().month < 6 else 'HS',
        }
