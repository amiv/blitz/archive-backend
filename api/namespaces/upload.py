# -*- coding: utf-8 -*-
import mimetypes
import os

from flask import current_app
from flask_restx import Namespace, Resource
from werkzeug.datastructures import FileStorage
from werkzeug.exceptions import BadRequest
from werkzeug.utils import secure_filename

from api.auth import login_required

from ..utils.files import create_preview, get_mimetype

api = Namespace('upload', description='Upload Files to the Archive')

upload_parser = api.parser()
upload_parser.add_argument('file', location='files', type=FileStorage, required=True)
upload_parser.add_argument('semester', location='form', type=str, required=True)
upload_parser.add_argument('year', location='form', type=int, required=True)
upload_parser.add_argument('edition_no', location='form', type=int, required=True)
upload_parser.add_argument('title', location='form', type=str, required=True)


@api.route('/')
class UploadEndpoint(Resource):
    @api.expect(upload_parser)
    @login_required(api)
    @api.doc(security='sessiontoken')
    def post(self):
        # Validate arguments
        args = upload_parser.parse_args()
        if not all(
            [args.get('file'), args.get('semester'), args.get('year'), args.get('edition_no'), args.get('title')]
        ):
            BadRequest('Incomplete request')

        upl_file: FileStorage = args['file']
        semester = secure_filename(args['semester'])
        year = secure_filename(str(args['year']))
        edition_no = int(args['edition_no'])
        title = secure_filename(args['title'])
        title = title.replace(' ', '_').replace('-', '–')

        if semester not in ['HS', 'FS']:
            BadRequest('Invalid Semester')

        dir_cfg = current_app.config['DATA_DIR']
        save_dir = os.path.join(dir_cfg, year, semester)
        if not os.path.isdir(save_dir):
            os.makedirs(save_dir)
            os.makedirs(os.path.join(save_dir, 'preview'))

        # check if filename is allowed
        filename_ext = secure_filename(upl_file.filename.split('.')[-1])
        if filename_ext not in current_app.config['ALLOWED_FILENAMES']:
            raise BadRequest('File extension not allowed')

        # check if MIME-Type of file matches the extension
        mimetype = get_mimetype(upl_file)

        if ('.' + filename_ext) not in mimetypes.guess_all_extensions(mimetype):
            raise BadRequest("Mimetype doesn't match to file extension")

        # save file
        save_fn = os.path.join(save_dir, f'blitz-{year}-{semester}-{edition_no:02d}-{title}.{filename_ext}')
        if os.path.isfile(save_fn):
            raise BadRequest('This blitz was already uploaded')
        upl_file.save(save_fn)

        # save preview
        save_prev_fn = os.path.join(save_dir, 'preview', f'blitz-{year}-{semester}-{edition_no:02d}-{title}.png')
        create_preview(save_fn, save_prev_fn)

        return {'message': 'Successfully saved'}, 202
