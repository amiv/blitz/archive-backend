# -*- coding: utf-8 -*-
import functools  # noqa: TAE001
from urllib.parse import unquote

from flask_restx import Namespace
from werkzeug.exceptions import Unauthorized

from api.auth.check_token import check_auth


def login_required(api: Namespace):  # noqa: ANN201
    def decorator(func):  # noqa: ANN001
        @functools.wraps(func)  # noqa: ANN201
        def wrapped(*f_args, **f_kwargs):  # noqa: ANN002, ANN003, ANN201
            parser = api.parser()
            parser.add_argument('Authorization', type=str, location='headers')
            token = parser.parse_args().get('Authorization', '')

            if check_auth(token):
                return func(*f_args, **f_kwargs)
            raise Unauthorized('Session Token missing, invalid login method, or not authorized')

        return wrapped

    return decorator
