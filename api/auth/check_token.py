# -*- coding: utf-8 -*-
from json import loads

import requests
from flask import current_app
from werkzeug.exceptions import BadRequest


def check_auth(token):
    req_group = current_app.config['REQUIRED_GROUP']
    try:
        api = requests.get(
            current_app.config['API_URL']
            + f'/groupmemberships?embedded={{"group":1}}&where={{"group":"{req_group}"}}',  # noqa: FS003
            headers={'Authorization': token},
        )
    except Exception as e:
        print(e)
        raise BadRequest('AMIV-API address misconfigured or unreachable', 500)
    try:
        string = api.text
        obj = loads(string)
        content = obj.get('_items', [])

        return any(
            group.get('group')
            and group.get('group').get('_id')
            and group['group']['_id'] == req_group
            for group in content
        )

    except Exception:
        raise BadRequest('AMIV-API returned unknown response.', 500)
