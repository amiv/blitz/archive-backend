# -*- coding: utf-8 -*-
import os
import re
from datetime import datetime

from flask import current_app, send_file
from flask_restx import Api, Resource
from werkzeug.exceptions import BadRequest
from werkzeug.utils import secure_filename

from api.auth import login_required

from .namespaces.overview import api as overview
from .namespaces.upload import api as upload

authorizations = {
    'sessiontoken': {'type': 'apiKey', 'in': 'header', 'name': 'Authorization'},
}


api = Api(
    title='Blitz Archive',
    description='CDN where data can be uploaded authenticated the AMIV groups',
    authorizations=authorizations,
)

api.add_namespace(upload)
api.add_namespace(overview)


def sanitize_blitz_id(blitz_id):
    try:
        blitz_id = re.sub(r'\.\.(\.)*', '', blitz_id)
        blitz_id = re.sub(r'\/*', '', blitz_id)
        splitted_name = blitz_id.split('-')
        semester = splitted_name[2]
        year = splitted_name[1]
        return year, semester, blitz_id
    except Exception:
        raise BadRequest('Requested blitz does not exist')


@api.route('/delete/<string:blitz_id>')
class Delete(Resource):
    @login_required(api)
    @api.doc(security='sessiontoken')
    def delete(self, blitz_id):
        year, semester, blitz_id = sanitize_blitz_id(blitz_id)

        date_string = datetime.now().strftime('%Y-%m-%dT%H-%M-%S-%f%z')
        data_dir = current_app.config['DATA_DIR']
        file_path = os.path.join(data_dir, year, semester, f'{blitz_id}.pdf')
        deleted_dir = os.path.join(data_dir, 'deleted')
        if not os.path.isdir(deleted_dir):
            os.makedirs(deleted_dir)
        file_del_path = os.path.join(deleted_dir, f'{blitz_id}-{date_string}.pdf')
        if not os.path.isfile(file_path):
            raise BadRequest('Requested blitz does not exist')

        file_prev_path = os.path.join(data_dir, year, semester, 'preview', f'{blitz_id}.png')
        os.rename(file_path, file_del_path)
        os.remove(file_prev_path)
        return {'message': 'Successfully deleted'}, 202


@api.route('/blitz/<string:blitz_id>')
class BlitzDownload(Resource):
    def get(self, blitz_id):
        year, semester, blitz_id = sanitize_blitz_id(blitz_id)

        data_dir = current_app.config['DATA_DIR']
        file_path = os.path.join(data_dir, year, semester, f'{blitz_id}.pdf')
        if not os.path.isfile(file_path):
            raise BadRequest('Requested blitz does not exist')
        return send_file(file_path, as_attachment=True)


@api.route('/preview/<string:blitz_id>')
class PreviewDownload(Resource):
    def get(self, blitz_id):
        year, semester, blitz_id = sanitize_blitz_id(blitz_id)

        data_dir = current_app.config['DATA_DIR']
        file_path = os.path.join(data_dir, year, semester, 'preview', f'{blitz_id}.png')
        if not os.path.isfile(file_path):
            raise BadRequest('Requested blitz does not exist')
        return send_file(file_path, as_attachment=True)
