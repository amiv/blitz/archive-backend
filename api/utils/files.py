# -*- coding: utf-8 -*-
import os.path
import uuid

import magic
from flask import current_app
from pdf2image import convert_from_path
from werkzeug.exceptions import BadRequest


def generate_filename(add_dir, extension):
    base_dir = current_app.config['BASE_DIR']
    while True:
        name = uuid.uuid1()
        filename = os.path.join(base_dir, add_dir, str(name) + '.' + extension)
        if not os.path.exists(filename):
            return filename


def get_mimetype(file_stream):
    """Get the mimetype from file data."""
    data = file_stream.stream.read()
    file_stream.stream.seek(0)
    file_analyzer = magic.Magic(mime=True)
    return file_analyzer.from_buffer(data)


def create_preview(pdf_file: str, save_png_file: str):
    try:
        images = convert_from_path(pdf_file, size=(512, None))
        images[0].save(save_png_file, 'PNG')
    except Exception as e:
        print(e)
        raise BadRequest('Error, saving file contact admin')


def fs_tree_to_dict(path_: str) -> dict:
    """Generates a dictionary representing the folder structure starting in the directory "path_"

    Args:
        path_ ([str], optional): path of dir in which to start creating the tree. Defaults to ROOT_DIR.

    Returns:
        dict: dict where the key correstponds to a directory name and value to its children.s
    """
    for root, dirs, files in os.walk(path_):
        dirs = filter(lambda d: d not in ['preview', 'deleted'], dirs)
        tree = {d: fs_tree_to_dict(os.path.join(root, d)) for d in dirs}
        files = map(lambda f: f.replace('.pdf', ''), filter(lambda f: not f.startswith('.'), files))
        tree.update({f: None for f in files})
        return tree
