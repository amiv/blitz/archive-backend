FROM python:3.10

RUN apt update && \
    apt install -y musl-dev python3-dev gcc git libev-dev poppler-utils && \
    pip install bjoern

COPY requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt && rm /tmp/requirements.txt


COPY . /api
WORKDIR /api

CMD ["python3", "-u", "server.py"]
