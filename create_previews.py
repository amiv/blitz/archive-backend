# -*- coding: utf-8 -*-
import os

from api.utils.files import create_preview

# delete all preview folders in the data directory


def create_previews():
    """Creates previews for all pdf files in the data directory."""
    for root, dirs, _files in os.walk('data'):
        dirs = filter(lambda d: d not in ['preview', 'deleted'], dirs)
        for dir_ in dirs:
            for root_, _dirs_, files_ in os.walk(os.path.join(root, dir_)):
                files_ = filter(lambda f: not f.startswith('.') and f.endswith('.pdf'), files_)
                for file_ in files_:
                    save_dir = os.path.join(root_, 'preview')
                    pdf_file = os.path.join(root_, file_)
                    png_file = os.path.join(save_dir, file_.replace('.pdf', '.png'))
                    if not os.path.exists(save_dir):
                        os.mkdir(save_dir)
                    if not os.path.isfile(png_file):
                        print(pdf_file)
                        create_preview(pdf_file, png_file)


# execute create_previews only if run as a script
if __name__ == '__main__':
    create_previews()
